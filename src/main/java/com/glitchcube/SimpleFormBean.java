package com.glitchcube;

import javax.enterprise.inject.Model;
import javax.validation.constraints.*;

/**
 * @author: glitchcube.com
 */
//Configure managed beans: https://docs.oracle.com/javaee/7/tutorial/jsf-configure001.htm#GIRCH
@Model //shorthand for @Named @javax.enterprise.context.RequestScoped
public class SimpleFormBean {

    //Bean validation: https://docs.oracle.com/javaee/7/tutorial/partbeanvalidation.htm#sthref1322
    @NotNull
    @Pattern(regexp = "[æøåÆØÅA-Za-z]*")
    private String name;

    private String land;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }
}
