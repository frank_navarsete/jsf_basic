package com.glitchcube;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;

/**
 * @author: glitchcube.com
 */
@Stateless //https://docs.oracle.com/javaee/7/tutorial/ejb-intro002.htm#GIPIN
@Named //Point 6.3.1.4 https://docs.oracle.com/javaee/7/tutorial/webapp003.htm#GLQLK
public class ServletContextBean {

    @Inject
    private ServletContext context;

    public String getServerInfo() {
        return context.getServerInfo();
    }

    public String getJsfVersion() {
        return FacesContext.class.getPackage().getImplementationVersion();
    }
}
