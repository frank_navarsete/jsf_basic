# README #

Run
	mvn clean package
Deploy
	target/JSFBasic.war
Open http://localhost:8080/JSFBasic/

The project is also available to run using Docker.

```bash
mvn clean package
docker build -t <name> . 
## Example docker build -t glitchcube/jsfbasic .
docker run  --name jsfbasic -d -p 8080:8080 glitchcube/jsfbasic
```

### What is this repository for? ###

* This project aims to give a brief introduction to JSF with Java EE 7. 
  Have tried to include reference to https://docs.oracle.com/javaee/7/tutorial/ where it is used.
* What you will see in the project is
- Template
- Create simple form
- Storing value in a java object (Bean)
- Include HTML5 with JSF (passthrough)
- Validation

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact