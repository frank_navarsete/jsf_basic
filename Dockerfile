FROM jboss/wildfly:10.1.0.Final

## Remove default welcome page
RUN rm -rf /opt/jboss/wildfly/welcome-content

## Add application to be run on wildfly
ADD target/JSFBasic.war /opt/jboss/wildfly/standalone/deployments/